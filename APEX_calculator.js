function calculateVal(){
    var errors = [];

    if(!checkValues()){errors.push('Были введены неверные значения (соответствующие поля отмечены красным цветом)');}

    var l = $('#room_len').val();
    var w = $('#room_wide').val();
    var h = $('#room_height').val();
    var s = l*w;

    var luks = $('#illumination_level').val();
    var vatt = $('#light_power').val();

    var safe_multiplier = $('#safe_multiplier').val();

    var h_multiplier = 1;

    if(h> 8){
        errors.push("Высота не может быть более 8 метров")
    }else if(h> 7){
        h_multiplier = 3.88;
    }else if(h> 6){
        h_multiplier = 3.35;
    }else if(h> 5){
        h_multiplier = 2.8;
    }else if(h> 4){
        h_multiplier = 2.2;
    }else if(h> 3){
        h_multiplier = 1.65;
    }

    var thisval = Math.ceil((s*luks*safe_multiplier/(vatt*100))*h_multiplier);

    if(!isNaN(thisval) && errors.length == 0){
        $('.calc-error').hide();
        $('.calc-image__light').show();
        $('.calc-results').show();
        $('#send').show();
        $('#calc_result').text(thisval);
    }else {
        $('.calc-results').hide();
        $('#send').hide();
        $('.calc-image__light').hide();
        $('#calc_result').text(0);
        showError(errors);
    }

}


function calculateS() {
    var l = $('#room_len').val();
    var w = $('#room_wide').val();
    var h = $('#room_hight').val();
    var s = l*w;

    if(!isNaN(s)){
        $('#area_m').show();
        $('#area_val').text(s);
    }else{
        $('#area_m').hide();
    }
}


function checkValues() {
    var rtrn = true;

    $('.js-check').each(function () {
        if(isNaN($(this).val())){
            $(this).addClass('valueAlert');
            rtrn = false;
        }else{
            $(this).removeClass('valueAlert');
        }

    });

    return rtrn;
}

function showError(errorText) {
    var errorHtml = "";
    if (typeof errorText === 'undefined' || errorText == "" || (Array.isArray(errorText) && errorText.length==0)) {
        errorHtml = "Были введены неверные значения (соответствующие поля отмечены красным цветом)";
    }
    if(typeof Array.isArray(errorText)){
        errorText.forEach(function (item, i, arr) {
            errorHtml += "<p>" + item + "</p>";
        });
    }

    $('.calc-error').html(errorHtml);
    $('.calc-error').show();

}

function hideError() {
    $('.calc-error').hide();
}