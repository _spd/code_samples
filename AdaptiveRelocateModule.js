/*

 Брэйкпоинты заданы в options.
 Чтобы элемент перемещался на определенном разрешении - назначаем ему data-relocate-XX="jQuerySelector", где XX - брейкпоинт
 Для того чтобы он возращался обратно - надо прописывать либо все брэйкпоинты, либо присвоить атрибут relocate-initial="jQuerySelector" -
 тогда на всех не указанных разрешениях он будет возвращаться в этот селектор.

 */

var adaptiveRelocate = function() {
    var options = {
        breakpoints: {
            // ascending order! or #TODO make auto sorting
            xs: 768,
            sm: 992,
            md: 1260,
            lg: 1330,
            vl: 1890
        }
    };

    var allSelector="";
    $.each(options.breakpoints, function (key, value) {
        allSelector+="[data-relocate-" + key + "], ";
    });
    allSelector = allSelector.slice(0, -2);

    relocate();

    $(window).resize(function(){
        relocate();
    });


    function relocate() {
        var currentWidth = $(window).outerWidth();
        var cR = currentRange(currentWidth);
        $("[data-relocate-" + cR + "]").each(function () {
            var this_container = $(this).data("relocate-" + cR);
            if(!$(this).parent().is(this_container)){
                $(this).appendTo($(this_container));
            }
        });

        $(allSelector).not("[data-relocate-" + cR + "]").filter("[data-relocate-initial]").each(function () {
            var this_container = $(this).data("relocate-initial");
            $(this).appendTo($(this_container));
        });

    }

    function currentRange(currentWidth) {
        var rtrn;
        $.each(options.breakpoints, function (key, value) {
            if(Number(currentWidth) <= Number(value)){
                rtrn = key;
                return false;
            }
        });
        return rtrn;
    }

};

module.exports = adaptiveRelocate;
